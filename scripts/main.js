// $(document).ready(() => {
//     $('.header').height($(window).height());
//     //manage submit
// })
const api = "https://api.themoviedb.org/3";
const key = "api_key=dae48bd01ef212b53f377c08cfc779f2";
const imgUrl = "https://image.tmdb.org/t/p/w500";
const movieStr = "search/movie";
async function eventSubmit(event, page) {
    console.log('prevent submit');
    event.preventDefault();

    page = 1;
    const strSearch = $('form input').val();
    //check input
    $('#main').empty();
    $('#extra').empty();
    $('#summary').empty();
    $(this).find("select").each(async() => {
        const selectedKey = $(this).find('option:selected').val();
        if (selectedKey == "film") {
            loading();

            const reqStr = `${api}/${movieStr}?query=${strSearch}&page=${page}&${key}`;
            console.log(reqStr);
            const response = await fetch(reqStr);
            const rs = await response.json();
            console.log(rs);

            fillMovies(rs.results);
        } else {
            loading();


            const rs = await getFilmsByCastname(strSearch);
            console.log(rs);
            if (rs.results[0] == undefined) {
                notFound();
            } else {
                fillMovies(rs.results[0].known_for);
            }
        }
    });





}

function notFound() {
    $('#main').empty();
    $('#extra').empty();
    $('#summary').empty();
    $('#main').append(`
    <div class="col-md-12">
        <h1>NOT FOUND</h1>
    </div>
    `)
}
async function eventDetail(event) {
    event.preventDefault();

    const idMovie = $(this).data("id");
    event.preventDefault();
    console.log(`click on tag <a> data-id ${idMovie}`);
    const reqFilmDetail = `${api}/movie/${idMovie}?${key}`;
    const reqPeopleOfFilm = `${api}/movie/${idMovie}/credits?${key}`;
    console.log(reqFilmDetail);
    loading();
    const res = await fetch(reqFilmDetail);
    const data = await res.json();
    const res2 = await fetch(reqPeopleOfFilm);
    const moreData = await res2.json();
    const reviews = await getReviews(idMovie);
    // console.log(data);
    // console.log(moreData);
    const director = getDirectorByFilmCrew(moreData.crew);
    //casts

    const genres = (data.genres).map(genre => genre.name);
    // console.log(genres);
    fillDetailMovie(data, director, genres);
    fillCasts(moreData.cast, 1, 5);
    fillReviews(reviews);


}



function fillMovies(movies) {
    $('#main').empty();
    $('#extra').empty();
    $('#summary').empty();
    // $('#reviews').empty();
    for (const movie of movies) {
        $('#main').append(`
        <div class="col-md-4">
        <div class="card h-100"  style="width: 20rem;">
            <div class="view overlay">
            <img class="card-img-top" src="${imgUrl}/${movie.poster_path}" alt="Card image cap">
            </div>
            <!-- Card content -->
            <div class="card-body film" >
                <!-- Title -->
                <h4 class="card-title font-weight-bold">
                    <a  href="#" class="aaf" data-id="${movie.id}">${movie.title}</a>
                </h4>
                <p class="card-text"><b>Vote average: </b>${movie.vote_average}</p>   
            </div>
        </div>
        </div>        
        `);
    }
};

function loading() {
    console.log('loading');
    $('#main').empty();
    $('#main').append(`
    <div class="col-md-12 d-flex justify-content-center">
        <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    `);
}
//startbootstrap.com
async function loadDetail(page) {
    page = parseInt(page) || 1;
    const reqStr = `${api}/trending/movie/week?${key}&page=${page}`;
    console.log(`reqStr = ${reqStr}`);

    loading();
    await fetch(reqStr)
        .then(async(res) => {
            const response = await res.json();
            const total_pages = response.total_pages;
            const total_result = response.total_results;
            const data = response.results;
            // console.log(data);
            fillMovies(data);
        })
        .catch(err => console.log(err));

}

async function testAPI() {
    const reqStr = `https://api.themoviedb.org/3/movie/429617?${key}`;
    // console.log(`click on tag <a> data-id ${idMovie}`);
    // const reqStr = `${api}/movie/${idMovie}?${key}`;
    const reqStr2 = 'https://api.themoviedb.org/3/movie/429617/credits?api_key=dae48bd01ef212b53f377c08cfc779f2';
    console.log(reqStr);
    loading();
    const res = await fetch(reqStr);
    const data = await res.json();
    const res2 = await fetch(reqStr2);
    const moreData = await res2.json();

    // console.log(data);
    // console.log(moreData);
    const director = getDirectorByFilmCrew(moreData.crew);
    //casts
    for (cast of moreData.cast) {
        console.log(cast);
    }
    const genres = (data.genres).map(genre => genre.name);
    // console.log(genres);
    fillDetailMovie2(data, director, genres);
    fillCasts(moreData.cast, 1, 6);
}

function getDirectorByFilmCrew(crewData) {
    for (crew of crewData) {
        if ((crew.job).toLowerCase() == "director") {
            return crew;
        }
    }

}

function fillCasts(casts, page, per_page) { //1,5 =>0*5
    $('#extra').empty();
    $('#extra').append(`<div class="card-header"><h3 style="color:#6ab446">Casts</h3></div>`);
    page = parseInt(page) || 1;
    per_page = parseInt(per_page) || 4;
    let indexStart = (page - 1) * per_page;
    for (let i = 0; i < per_page; i++) {
        let cast = casts[indexStart + i];
        console.log(`${casts[i*page].name} index = ${i*page}`);



        $('#extra').append(`
        
        <div class="card col-md-2 py-4 " style="width: 25rem;">
            <img class="card-img-top thumbnail img-responsive" src="${imgUrl}/${cast.profile_path}" alt="profile of ${cast.name}" >
            <div class="card-body">
                <h4 class="card-title font-weight-bold">
                    <a  href="#" class="castCard" data-id="${cast.id}">${cast.name}</a>
                </h4>
                <p class="card-text"><b>Character</b>: ${cast.character}</p>
            </div>
      
        </div>
       
       
        `);
    }
}



function fillDetailMovie(movie, director, genres) {
    const date = new Date(movie.release_date);
    console.log(date.getFullYear());

    $('#main').empty();
    $('#extra').empty();
    // $('#reviews').empty();
    $('#main').append(`
    
    <div class="col-md-4">
            <div class="card-body ">
                <div class="view overlay">
                <img class="card-img-left" src="${imgUrl}/${movie.backdrop_path}" alt="Card image cap">

                </div>

            <!-- Card content -->
           
            
            </div>
    </div>  
    <div class="col-md-4 mx-auto">
        <div class="card-body" >

            <!-- Title -->
            <h4 class="card-title font-weight-bold">
            ${movie.title}
            </h4>


            <p><b>Release year: </b><span class="release_date">${date.getFullYear()}</span></p>
            <p><b>Director: </b>${director.name}</p>
            <p><b>Overview: </b>${movie.overview}</p>

            <br><p class="movie-dt genres"><b>Genres: </b></p>         
        </div>
    </div>  

           

        `);
    const genresLength = genres.length;
    for (let i = 0; i < genresLength - 1; i++) {
        $('.genres').append(`
            <span>${genres[i]}, </span>
        `);
    }
    $('.genres').append(`
    <span>${genres[genresLength-1]}</span>
`);
}
async function getFilmsByCastname(name) {
    const reqStr = `${api}/search/person?${key}&query=${name}`;
    console.log(reqStr);
    const res = await fetch(reqStr);
    return await res.json();
}

function eventDetailCast(event) {
    event.preventDefault();
    const castId = $(this).data('id');
    // alert(`click on castId = ${castId}`);
    getCastByCastId(castId);
}
async function getCastByCastId(castId) {


    const reqStr1 = `${api}/person/${castId}?${key}`;
    console.log(`get cast by castId = ${castId}, req = ${reqStr1}`);
    loading();
    const res1 = await fetch(reqStr1);
    const cast = await res1.json();
    console.log(`${cast}`);
    const reqStr2 = `${api}/person/${castId}/movie_credits?${key}`;
    console.log(`get relevant movie by castId,  req = ${reqStr2}`);

    const res2 = await fetch(reqStr2);
    const creditsCast = await res2.json();
    fillCast(cast);


    // console.log(creditsCast);
    fillRelevantMovie(creditsCast.cast);
}

function fillCast(cast) {
    $('#main').empty();
    $('#extra').empty();
    $('#summary').empty();
    $('#main').append(` <div class="col-4 ">
    <div class="card-body ">
        <div class="view overlay">
        <img class="card-img-left img-thumbnail" src="${imgUrl}/${cast.profile_path}" alt="profile of ${cast.name}">

        </div>

        <!-- Card content -->
   
    
        </div>
        </div>  
        <div class="col-8 mx-auto p-0">
            <div class="card-body" >

        <!-- Title -->
        <h4 class="card-title font-weight-bold">
            ${cast.name}
        </h4>


       
        <p><b>Biography: </b>${cast.biography}</p>
      
        </div>
        </div>  

   

        `);
}

function fillRelevantMovie(creditsCast) {
    // $('#extra').append(`
    // <p><h2>Films related to cast</h2></p>
    // </br>
    // `);
    let start = 0;
    for (let i = start; i < start + 8 && i < creditsCast.length; i++) {
        let cast = creditsCast[i];
        console.log(cast);
        $('#extra').append(`
        
        <div class="card col-md-3 py-4 " style="width: 25rem;">
        <img class="card-img-top thumbnail img-responsive" src="${imgUrl}/${cast.backdrop_path}" alt="profile of ${cast.title}" >
        <div class="card-body">
            <h4 class="card-title font-weight-bold">
                ${cast.title}
            </h4>
            <p class="card-text"><b>Character</b>: ${cast.character}</p>
            <p class="card-text"><b>Overview</b>: ${cast.overview}</p>
        </div>
  
    </div>
       
        `)
    }

}
async function getReviews(movieId) {
    const reqStr = `${api}/movie/${movieId}/reviews?${key}`;
    const res = await fetch(reqStr);
    const reviews = await res.json();
    console.log(reviews);
    return reviews.results;
}

function fillReviews(reviews) {
    $('#summary').append(` <div class = "card-header" >
        <h2 style = "color:#6ab446" > Reviews </h2> </div>
        `);
    for (review of reviews) {
        console.log(review);
        $('#summary').append(`
            <div class="card">
            <p class="card-text">
                <b><h4>Written by <span style="color:#007bff">${review.author}<span></h4></b>
            </p>
            <p class="card-text collapse" id="collapse${review.id}">${review.content}</p>
            <a class="collapsed" data-toggle="collapse" href="#collapse${review.id}" 
                aria-expanded="false" aria-controls="collapse${review.id}"></a>
           </div>
            
        `);
    }

}